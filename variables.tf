variable "name" {
    description = "Name for the Resource Group "
    default     = "test-rg"
}

variable "location" {
    description = "Location  "
    default     = "eastus"
}

variable "env" {
    description = "Enviornment  "
    default     = "test"
}